# COVID19 #


Esse repositório apresenta dados sobre número de casos de coronavírus no Brasil.


### Dados ###
Os dados utilizados foram extraidos dos repositorios:
* Secretarias de Saúde das Unidades Federativas.
* Brasil.IO.
* G1 portal Globo.

### Feramentas e Biblioteca ###

* Jupyter Notebook
* Pandas
* Matplotlib 
* Plotly


### Apresentação dos Dados ###

* Gráficos
* Tabelas


### O que é exibido? ###

* Casos de coronavírus confirmados no brasil.
* Casos de coronavírus confirmados por região.
* Casos de coronavírus confirmados por estado e DF.
* Casos de coronavírus confirmados e mortes.
* Casos de coronavírus confirmados Município de Alegrete,rs.